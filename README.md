## Drones

---

## Requirements
- Maven: 3.9
- Java: 19

---

## Running the project

The project can be built and run with maven using the following (in the root directory of the project):

```sh
mvn clean spring-boot:run 
```

The project can then be accessed from url: localhost:8080

The following endpoints can be accessed.

| ENDPOINT                           | URL                                         | METHOD | PAYLOAD (JSON)                                                                                              |
|------------------------------------|---------------------------------------------|--------|-------------------------------------------------------------------------------------------------------------|
| Available Drones                   | localhost:8080/drones                       | GET    |                                                                                                             |
| Register Drone                     | localhost:8080/drones                       | POST   | {"serial_number" : "JS1234KL", "model" : Model.LIGHTWEIGHT, "weight_limit" : 200, "battery_capacity" : 100} | 
| Load Drone                         | localhost:8080/drones/{id}/load_medication  | POST   | {"id" : 1, "quantity" : 10}                                                                                 |
| Check Drone Medication             | localhost:8080/drones/{id}/check_load       | GET    |                                                                                                             |
| Check Drone Battery Level          | localhost:8080/drones/{id}/check_battery    | GET    |                                                                                                             |
| Audit Log for drone battery levels | localhost:8080/drone_battery_level_logs     | GET    |                                                                                                             |

---

## Running the tests
The project tests can be run using the following:

```sh
mvn test -Dtest="DroneApiApplicationTests"
mvn test -Dtest="RepositoryTests"
mvn test -Dtest="DispatchControllerTests"
```

---

## Sample data loaded in the database

- Drone("JS1234KL", Model.LIGHTWEIGHT, 200, 100, State.IDLE)
- Drone("JS5463NM", Model.MIDDLEWEIGHT, 300, 100, State.IDLE)
- Drone("JS4390BC", Model.CRUISERWEIGHT, 400, 100, State.IDLE)
- Drone("JS1298VH", Model.HEAVYWEIGHT, 500, 100, State.IDLE)
- Medication("Amoxicillin", 10, "AM109283", "amoxicillin_capsule_500.jpeg")
- Medication("Aspirin", 5, "M893947", "aspirin.jpeg")
- Medication("Baclofen", 20, "M868695", "baclof-10mg-tablet.jpeg")
- Medication("Ciprofloxacin", 1.5, "M957499", "ciprofloxacin-1.png")
- Medication("Diazepam", 5.5, "M635473", "diazepam.png")
- Medication("Hydrocortisone", 3, "M1053647", "AXCEL-HYDROCORTISONE-CREAM-15G.jpeg")
- Medication("Hydroxychloroquine", 2, "M225346", "hydroxychloroquine.jpeg")
- Medication("Ibuprofen", 8, "M223412", "Ibuprofen.png")
- Medication("Memantine", 4, "M2637829", "memantine.max-1800x1800.png")
- Medication("Zopiclone", 1, "M1234563", "Zopiclone.jpeg")

### Drone Battery Level Audit Logs

A periodic task executed every three minutes takes logs of all the drone battery levels and stores them in a table. The logs can be accessed through the link: localhost:8080/drone_battery_level_logs 