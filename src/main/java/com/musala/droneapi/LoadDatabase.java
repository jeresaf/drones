package com.musala.droneapi;

import com.musala.droneapi.constants.Model;
import com.musala.droneapi.constants.State;
import com.musala.droneapi.entities.Drone;
import com.musala.droneapi.entities.Medication;
import com.musala.droneapi.repositories.DroneRepository;
import com.musala.droneapi.repositories.MedicationRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class LoadDatabase {

    private static final Logger log = LoggerFactory.getLogger(LoadDatabase.class);

    @Bean
    CommandLineRunner initDatabase(MedicationRepository medicationRepository, DroneRepository droneRepository) {

        return args -> {
            //Load drones
            log.info("Preloading " + droneRepository.save(new Drone("JS1234KL", Model.LIGHTWEIGHT, 200, 100, State.IDLE)));
            log.info("Preloading " + droneRepository.save(new Drone("JS5463NM", Model.MIDDLEWEIGHT, 300, 100, State.IDLE)));
            log.info("Preloading " + droneRepository.save(new Drone("JS4390BC", Model.CRUISERWEIGHT, 400, 100, State.IDLE)));
            log.info("Preloading " + droneRepository.save(new Drone("JS1298VH", Model.HEAVYWEIGHT, 500, 100, State.IDLE)));

            //load medications
            log.info("Preloading " + medicationRepository.save(new Medication("Amoxicillin", 10, "AM109283", "amoxicillin_capsule_500.jpeg")));
            log.info("Preloading " + medicationRepository.save(new Medication("Aspirin", 5, "M893947", "aspirin.jpeg")));
            log.info("Preloading " + medicationRepository.save(new Medication("Baclofen", 20, "M868695", "baclof-10mg-tablet.jpeg")));
            log.info("Preloading " + medicationRepository.save(new Medication("Ciprofloxacin", 1.5, "M957499", "ciprofloxacin-1.png")));
            log.info("Preloading " + medicationRepository.save(new Medication("Diazepam", 5.5, "M635473", "diazepam.png")));
            log.info("Preloading " + medicationRepository.save(new Medication("Hydrocortisone", 3, "M1053647", "AXCEL-HYDROCORTISONE-CREAM-15G.jpeg")));
            log.info("Preloading " + medicationRepository.save(new Medication("Hydroxychloroquine", 2, "M225346", "hydroxychloroquine.jpeg")));
            log.info("Preloading " + medicationRepository.save(new Medication("Ibuprofen", 8, "M223412", "Ibuprofen.png")));
            log.info("Preloading " + medicationRepository.save(new Medication("Memantine", 4, "M2637829", "memantine.max-1800x1800.png")));
            log.info("Preloading " + medicationRepository.save(new Medication("Zopiclone", 1, "M1234563", "Zopiclone.jpeg")));
        };
    }

}
