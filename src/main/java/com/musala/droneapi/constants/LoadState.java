package com.musala.droneapi.constants;

public enum LoadState {
    LOADED,
    DELIVERED
}
