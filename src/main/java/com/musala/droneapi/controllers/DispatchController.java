package com.musala.droneapi.controllers;

import java.util.List;
import java.util.stream.Collectors;

import com.musala.droneapi.constants.LoadState;
import com.musala.droneapi.constants.State;
import com.musala.droneapi.entities.Drone;
import com.musala.droneapi.entities.DroneBatteryLevelAuditLog;
import com.musala.droneapi.entities.DroneMedicationLoad;
import com.musala.droneapi.entities.Medication;
import com.musala.droneapi.entities.assemblers.DroneBatteryLevelAuditLogModelAssembler;
import com.musala.droneapi.entities.assemblers.DroneMedicationLoadModelAssembler;
import com.musala.droneapi.entities.assemblers.DroneModelAssembler;
import com.musala.droneapi.entities.assemblers.MedicationModelAssembler;
import com.musala.droneapi.entities.projections.DroneBatteryLevel;
import com.musala.droneapi.entities.projections.DroneMedication;
import com.musala.droneapi.exceptions.*;
import com.musala.droneapi.models.request.LoadMedicationRequest;
import com.musala.droneapi.models.request.RegisterDroneRequest;
import com.musala.droneapi.repositories.DroneBatteryLevelAuditLogRepository;
import com.musala.droneapi.repositories.DroneMedicationLoadRepository;
import com.musala.droneapi.repositories.DroneRepository;
import com.musala.droneapi.repositories.MedicationRepository;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.IanaLinkRelations;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
public class DispatchController {

    private final DroneRepository droneRepository;
    private final DroneModelAssembler droneModelAssembler;
    private final MedicationRepository medicationRepository;
    private final MedicationModelAssembler medicationModelAssembler;
    private final DroneMedicationLoadRepository droneMedicationLoadRepository;
    private final DroneMedicationLoadModelAssembler droneMedicationLoadModelAssembler;
    private final DroneBatteryLevelAuditLogRepository droneBatteryLevelAuditLogRepository;
    private final DroneBatteryLevelAuditLogModelAssembler droneBatteryLevelAuditLogModelAssembler;

    public DispatchController(DroneRepository droneRepository, DroneModelAssembler droneModelAssembler, MedicationRepository medicationRepository,
                              MedicationModelAssembler medicationModelAssembler, DroneMedicationLoadRepository droneMedicationLoadRepository,
                              DroneMedicationLoadModelAssembler droneMedicationLoadModelAssembler, DroneBatteryLevelAuditLogRepository droneBatteryLevelAuditLogRepository,
                              DroneBatteryLevelAuditLogModelAssembler droneBatteryLevelAuditLogModelAssembler) {
        this.droneRepository = droneRepository;
        this.droneModelAssembler = droneModelAssembler;
        this.medicationRepository = medicationRepository;
        this.medicationModelAssembler = medicationModelAssembler;
        this.droneMedicationLoadRepository = droneMedicationLoadRepository;
        this.droneMedicationLoadModelAssembler = droneMedicationLoadModelAssembler;
        this.droneBatteryLevelAuditLogRepository = droneBatteryLevelAuditLogRepository;
        this.droneBatteryLevelAuditLogModelAssembler = droneBatteryLevelAuditLogModelAssembler;
    }

    @PostMapping("/drones")
    ResponseEntity<?> registerDrone(@RequestBody RegisterDroneRequest registerDroneRequest) {
        if(registerDroneRequest.getSerial_number() == null || registerDroneRequest.getSerial_number().isBlank() ||
            registerDroneRequest.getSerial_number().length() > 100) {
            throw new InvalidInputException("Serial number is required and should be at most 100 characters");
        } else if(registerDroneRequest.getWeight_limit() <= 0 || registerDroneRequest.getWeight_limit() > 500) {
            throw new InvalidInputException("Weight limit is required and should be be any value from 1 to 500");
        } else if(registerDroneRequest.getBattery_capacity() < 0 || registerDroneRequest.getBattery_capacity() > 100) {
            throw new InvalidInputException("Weight limit is required and should be be any value from 0 to 100");
        }
        Drone drone = new Drone(registerDroneRequest.getSerial_number(), registerDroneRequest.getModel(), registerDroneRequest.getWeight_limit(),
                registerDroneRequest.getBattery_capacity(), State.IDLE);

        EntityModel<Drone> entityModel = droneModelAssembler.toModel(droneRepository.save(drone));

        return ResponseEntity
                .created(entityModel.getRequiredLink(IanaLinkRelations.SELF).toUri()) //
                .body(entityModel);
    }

    @PostMapping("/drones/{id}/load_medication")
    ResponseEntity<?> loadDrone(@RequestBody LoadMedicationRequest loadMedicationRequest, @PathVariable Long id) {

        Drone drone = droneRepository.findById(id)
                .orElseThrow(() -> new DroneNotFoundException(id));

        if(drone.getState() != State.LOADING && drone.getState() != State.IDLE) {
            throw new InvalidInputException("Drone cannot be loaded with more medication");
        }

        if(drone.getBattery_capacity() < 25) {
            throw new InvalidInputException("Drone cannot be loaded with medication, battery level less than 25%");
        }

        Medication medication = medicationRepository.findById(loadMedicationRequest.getId())
                .orElseThrow(() -> new MedicationNotFoundException(loadMedicationRequest.getId()));

        List<DroneMedicationLoad> droneMedicationLoadList = droneMedicationLoadRepository.findByDroneAndMedicationAndLoadState(drone, medication, LoadState.LOADED);

        DroneMedicationLoad droneMedicationLoad = null;
        int previousQuantity = 0;
        if(!droneMedicationLoadList.isEmpty()) {
            /*
            if(droneMedicationLoadList.size() > 1) {
                //Drone medication load should ideally never be in this state
                //Error to show that a medication can only be loaded state once
            }
            */
            droneMedicationLoad = droneMedicationLoadList.get(0);
            previousQuantity = droneMedicationLoad.getQuantity();
        }

        double totalCurrentWeightOnDrone = 0;
        List<DroneMedicationLoad> allDroneMedicationLoadList = droneMedicationLoadRepository.findByDroneAndLoadState(drone, LoadState.LOADED);
        for (DroneMedicationLoad _droneMedicationLoad: allDroneMedicationLoadList) {
            int _quantity = _droneMedicationLoad.getQuantity();
            double _medicationWeight = _droneMedicationLoad.getMedication().getWeight();
            totalCurrentWeightOnDrone += _medicationWeight * (double) _quantity;
        }


        int quantity = loadMedicationRequest.getQuantity() + previousQuantity;
        double medicationWeight = medication.getWeight();
        double totalWeight = medicationWeight * (double) quantity;

        if((totalCurrentWeightOnDrone + totalWeight) > drone.getWeight_limit()) {
            throw new InvalidInputException("Medication weight exceeds drone weight limit, current weight on drone is " + totalCurrentWeightOnDrone + ", additional weight of " + totalWeight + " cannot be added");
        }

        if(droneMedicationLoad == null) {
            droneMedicationLoad = new DroneMedicationLoad(drone, medication, LoadState.LOADED, quantity);
        } else {
            droneMedicationLoad.setQuantity(quantity);
        }
        droneMedicationLoad = droneMedicationLoadRepository.save(droneMedicationLoad);

        EntityModel<DroneMedicationLoad> entityModel = droneMedicationLoadModelAssembler.toModel(droneMedicationLoad);

        return ResponseEntity
                .created(entityModel.getRequiredLink(IanaLinkRelations.SELF).toUri()) //
                .body(entityModel);

    }

    @GetMapping("/drones/{id}/check_load")
    CollectionModel<EntityModel<DroneMedication>> checkLoadedMedication(@PathVariable Long id) {

        Drone drone = droneRepository.findById(id)
                .orElseThrow(() -> new DroneNotFoundException(id));
        List<EntityModel<DroneMedication>> entityModelList = droneMedicationLoadRepository.findDroneMedicationLoad(drone, LoadState.LOADED).stream() //
                .map(droneMedication -> EntityModel.of(droneMedication,
                        linkTo(methodOn(DispatchController.class).getDroneMedicationLoad(id)).withSelfRel(),
                        linkTo(methodOn(DispatchController.class).allDroneMedicationLoads()).withRel("drone_medication_loads"))) //
                .collect(Collectors.toList());
        return CollectionModel.of(entityModelList, linkTo(methodOn(DispatchController.class).allDroneMedicationLoads()).withSelfRel());
    }

    @GetMapping("/drones")
    public CollectionModel<EntityModel<Drone>> availableDrones() {
        List<EntityModel<Drone>> entityModelList = droneRepository.findByState(State.IDLE).stream()
                .map(droneModelAssembler::toModel)
                .collect(Collectors.toList());
        return CollectionModel.of(entityModelList, linkTo(methodOn(DispatchController.class).allDrones()).withSelfRel());
    }

    @GetMapping("/drones/{id}/check_battery")
    EntityModel<DroneBatteryLevel> checkDroneBatteryLevel(@PathVariable Long id) {

        DroneBatteryLevel droneBatteryLevel = droneRepository.findDroneBatteryCapacity(id);
        return EntityModel.of(droneBatteryLevel,
                linkTo(methodOn(DispatchController.class).checkDroneBatteryLevel(id)).withSelfRel(),
                linkTo(methodOn(DispatchController.class).checkLoadedMedication(id)).withRel("drones/" + id + "/check_load"),
                linkTo(methodOn(DispatchController.class).availableDrones()).withRel("drones"));
    }




    @GetMapping("/drone_battery_level_logs")
    public CollectionModel<EntityModel<DroneBatteryLevelAuditLog>> allDroneBatteryLevelLogs() {
        List<EntityModel<DroneBatteryLevelAuditLog>> entityModelList = droneBatteryLevelAuditLogRepository.findAll().stream()
                .map(droneBatteryLevelAuditLogModelAssembler::toModel)
                .collect(Collectors.toList());
        return CollectionModel.of(entityModelList, linkTo(methodOn(DispatchController.class).allDroneBatteryLevelLogs()).withSelfRel());
    }

    @GetMapping("/drone_battery_level_logs/{id}")
    public EntityModel<DroneBatteryLevelAuditLog> getDroneBatteryLevelLogs(@PathVariable Long id) {

        DroneBatteryLevelAuditLog droneBatteryLevelAuditLog = droneBatteryLevelAuditLogRepository.findById(id)
                .orElseThrow(() -> new DroneBatteryLevelAuditLogNotFoundException(id));

        return droneBatteryLevelAuditLogModelAssembler.toModel(droneBatteryLevelAuditLog);
    }

    @GetMapping("/drone_battery_level_logs/drone/{id}")
    CollectionModel<EntityModel<DroneBatteryLevelAuditLog>> getDroneBatteryLevelLogsForDrone(@PathVariable Long id) {

        Drone drone = droneRepository.findById(id)
                .orElseThrow(() -> new DroneNotFoundException(id));

        List<EntityModel<DroneBatteryLevelAuditLog>> entityModelList = droneBatteryLevelAuditLogRepository.findByDrone(drone).stream() //
                .map(droneBatteryLevelAuditLogModelAssembler::toModel) //
                .collect(Collectors.toList());
        return CollectionModel.of(entityModelList, linkTo(methodOn(DispatchController.class).getDroneBatteryLevelLogsForDrone(drone.getId())).withSelfRel());
    }





    @GetMapping("/drone_medication_loads")
    public CollectionModel<EntityModel<DroneMedicationLoad>> allDroneMedicationLoads() {
        List<EntityModel<DroneMedicationLoad>> entityModelList = droneMedicationLoadRepository.findAll().stream() //
                .map(droneMedicationLoadModelAssembler::toModel) //
                .collect(Collectors.toList());
        return CollectionModel.of(entityModelList, linkTo(methodOn(DispatchController.class).allDroneMedicationLoads()).withSelfRel());
    }

    @GetMapping("/drone_medication_loads/{id}")
    public EntityModel<DroneMedicationLoad> getDroneMedicationLoad(@PathVariable Long id) {

        DroneMedicationLoad droneMedicationLoad = droneMedicationLoadRepository.findById(id)
                .orElseThrow(() -> new DroneMedicationLoadNotFoundException(id));
        return droneMedicationLoadModelAssembler.toModel(droneMedicationLoad);
    }





    @GetMapping("/all_drones")
    public CollectionModel<EntityModel<Drone>> allDrones() {
        List<EntityModel<Drone>> modelList = droneRepository.findAll().stream()
                .map(droneModelAssembler::toModel)
                .collect(Collectors.toList());
        return CollectionModel.of(modelList, linkTo(methodOn(DispatchController.class).allDrones()).withSelfRel());
    }

    @PostMapping("/new_drones")
    ResponseEntity<?> newDrone(@RequestBody Drone newDrone) {
        EntityModel<Drone> entityModel = droneModelAssembler.toModel(droneRepository.save(newDrone));

        return ResponseEntity
                .created(entityModel.getRequiredLink(IanaLinkRelations.SELF).toUri()) //
                .body(entityModel);
    }

    @GetMapping("/drones/{id}")
    public EntityModel<Drone> getDrone(@PathVariable Long id) {
        Drone drone = droneRepository.findById(id)
                .orElseThrow(() -> new DroneNotFoundException(id));
        return droneModelAssembler.toModel(drone);
    }

    @PutMapping("/drones/{id}")
    ResponseEntity<?> replaceDrone(@RequestBody Drone newDrone, @PathVariable Long id) {

        Drone updatedDrone = droneRepository.findById(id)
                .map(drone -> {
                    drone.setSerial_number(newDrone.getSerial_number());
                    drone.setModel(newDrone.getModel());
                    drone.setWeight_limit(newDrone.getWeight_limit());
                    drone.setBattery_capacity(newDrone.getBattery_capacity());
                    drone.setState(newDrone.getState());
                    return droneRepository.save(drone);
                })
                .orElseGet(() -> {
                    newDrone.setId(id);
                    return droneRepository.save(newDrone);
                });

        EntityModel<Drone> entityModel = droneModelAssembler.toModel(updatedDrone);

        return ResponseEntity //
                .created(entityModel.getRequiredLink(IanaLinkRelations.SELF).toUri()) //
                .body(entityModel);
    }

    @DeleteMapping("/drones/{id}")
    ResponseEntity<?> deleteDrone(@PathVariable Long id) {
        droneRepository.deleteById(id);
        return ResponseEntity.noContent().build();
    }




    @GetMapping("/medications")
    public CollectionModel<EntityModel<Medication>> allMedications() {
        List<EntityModel<Medication>> employees = medicationRepository.findAll().stream()
                .map(medicationModelAssembler::toModel)
                .collect(Collectors.toList());
        return CollectionModel.of(employees, linkTo(methodOn(DispatchController.class).allMedications()).withSelfRel());
    }

    @PostMapping("/medications")
    ResponseEntity<?> newMedication(@RequestBody Medication newMedication) {
        EntityModel<Medication> entityModel = medicationModelAssembler.toModel(medicationRepository.save(newMedication));

        return ResponseEntity
                .created(entityModel.getRequiredLink(IanaLinkRelations.SELF).toUri()) //
                .body(entityModel);
    }

    @GetMapping("/medications/{id}")
    public EntityModel<Medication> getMedication(@PathVariable Long id) {

        Medication medication = medicationRepository.findById(id)
                .orElseThrow(() -> new MedicationNotFoundException(id));
        return medicationModelAssembler.toModel(medication);
    }

    @PutMapping("/medications/{id}")
    ResponseEntity<?> replaceMedication(@RequestBody Medication newMedication, @PathVariable Long id) {

        Medication updatedMedication = medicationRepository.findById(id)
                .map(medication -> {
                    medication.setName(newMedication.getName());
                    medication.setWeight(newMedication.getWeight());
                    medication.setCode(newMedication.getCode());
                    medication.setImage(newMedication.getImage());
                    return medicationRepository.save(medication);
                })
                .orElseGet(() -> {
                    newMedication.setId(id);
                    return medicationRepository.save(newMedication);
                });

        EntityModel<Medication> entityModel = medicationModelAssembler.toModel(updatedMedication);

        return ResponseEntity //
                .created(entityModel.getRequiredLink(IanaLinkRelations.SELF).toUri()) //
                .body(entityModel);
    }

    @DeleteMapping("/medications/{id}")
    ResponseEntity<?> deleteMedication(@PathVariable Long id) {
        medicationRepository.deleteById(id);
        return ResponseEntity.noContent().build();
    }

}
