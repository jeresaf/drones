package com.musala.droneapi.controllers.advice;

import com.musala.droneapi.exceptions.DroneBatteryLevelAuditLogNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class DroneBatteryLevelAuditLogNotFoundAdvice {
    @ResponseBody
    @ExceptionHandler(DroneBatteryLevelAuditLogNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    String droneBatteryLevelAuditLogNotFoundHandler(DroneBatteryLevelAuditLogNotFoundException ex) {
        return ex.getMessage();
    }
}
