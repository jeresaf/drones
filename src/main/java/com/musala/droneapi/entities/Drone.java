package com.musala.droneapi.entities;

import com.musala.droneapi.constants.Model;
import com.musala.droneapi.constants.State;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;

import java.util.Objects;

@Entity
public class Drone {

	private @Id
	@GeneratedValue Long id;
	private String serial_number;
	private Model model;
	private double weight_limit;
	private int battery_capacity;
	private State state;

	public Drone() {}

	public Drone(String serial_number, Model model, double weight_limit, int battery_capacity, State state) {
		this.serial_number = serial_number;
		this.model = model;
		this.weight_limit = weight_limit;
		this.battery_capacity = battery_capacity;
		this.state = state;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSerial_number() {
		return serial_number;
	}

	public void setSerial_number(String serial_number) {
		this.serial_number = serial_number;
	}

	public Model getModel() {
		return model;
	}

	public void setModel(Model model) {
		this.model = model;
	}

	public double getWeight_limit() {
		return weight_limit;
	}

	public void setWeight_limit(double weight_limit) {
		this.weight_limit = weight_limit;
	}

	public int getBattery_capacity() {
		return battery_capacity;
	}

	public void setBattery_capacity(int battery_capacity) {
		this.battery_capacity = battery_capacity;
	}

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Drone drone = (Drone) o;
		return id.equals(drone.id) && serial_number.equals(drone.serial_number) && model == drone.model && weight_limit== drone.weight_limit && battery_capacity == drone.battery_capacity && state == drone.state;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, serial_number, model, weight_limit, battery_capacity, state);
	}

	@Override
	public String toString() {
		return "Drone{" +
				"id=" + id +
				", serial_number='" + serial_number + '\'' +
				", model=" + model +
				", weight_limit='" + weight_limit + '\'' +
				", battery_capacity='" + battery_capacity + '\'' +
				", state=" + state +
				'}';
	}
}
