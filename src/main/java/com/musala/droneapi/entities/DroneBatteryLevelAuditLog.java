package com.musala.droneapi.entities;

import jakarta.persistence.*;

import java.util.Objects;

@Entity
public class DroneBatteryLevelAuditLog {

    private @Id
    @GeneratedValue Long id;

    @ManyToOne
    @JoinColumn(name = "drone_id")
    private Drone drone;

    private int battery_capacity;

    public DroneBatteryLevelAuditLog() {}

    public DroneBatteryLevelAuditLog(Drone drone, int battery_capacity) {
        this.drone = drone;
        this.battery_capacity = battery_capacity;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Drone getDrone() {
        return drone;
    }

    public void setDrone(Drone drone) {
        this.drone = drone;
    }

    public int getBattery_capacity() {
        return battery_capacity;
    }

    public void setBattery_capacity(int battery_capacity) {
        this.battery_capacity = battery_capacity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DroneBatteryLevelAuditLog that = (DroneBatteryLevelAuditLog) o;
        return battery_capacity == that.battery_capacity && id.equals(that.id) && drone.equals(that.drone);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, drone, battery_capacity);
    }

    @Override
    public String toString() {
        return "DroneBatteryLevelAuditLog{" +
                "id=" + id +
                ", drone=" + drone +
                ", battery_capacity=" + battery_capacity +
                '}';
    }
}
