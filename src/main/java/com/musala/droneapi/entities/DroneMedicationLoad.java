package com.musala.droneapi.entities;

import com.musala.droneapi.constants.LoadState;

import jakarta.persistence.*;

import java.util.Objects;

@Entity
public class DroneMedicationLoad {

    private @Id @GeneratedValue Long id;

    @ManyToOne
    @JoinColumn(name = "drone_id")
    private Drone drone;

    @ManyToOne
    @JoinColumn(name = "medication_id")
    private Medication medication;

    private LoadState loadState;

    private int quantity;

    public DroneMedicationLoad() {}

    public DroneMedicationLoad(Drone drone, Medication medication, LoadState loadState, int quantity) {
        this.drone = drone;
        this.medication = medication;
        this.loadState = loadState;
        this.quantity = quantity;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Drone getDrone() {
        return drone;
    }

    public void setDrone(Drone drone) {
        this.drone = drone;
    }

    public Medication getMedication() {
        return medication;
    }

    public void setMedication(Medication medication) {
        this.medication = medication;
    }

    public LoadState getLoadState() {
        return loadState;
    }

    public void setLoadState(LoadState loadState) {
        this.loadState = loadState;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DroneMedicationLoad that = (DroneMedicationLoad) o;
        return id.equals(that.id) && drone.equals(that.drone) && medication.equals(that.medication) && loadState == that.loadState && quantity == that.quantity;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, drone, medication, loadState, quantity);
    }

    @Override
    public String toString() {
        return "DroneMedicationLoad{" +
                "id=" + id +
                ", drone=" + drone +
                ", medication=" + medication +
                ", loadState=" + loadState +
                ", quantity=" + quantity +
                '}';
    }
}
