package com.musala.droneapi.entities.assemblers;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.*;

import com.musala.droneapi.controllers.DispatchController;
import com.musala.droneapi.entities.DroneBatteryLevelAuditLog;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

@Component
public class DroneBatteryLevelAuditLogModelAssembler implements RepresentationModelAssembler<DroneBatteryLevelAuditLog, EntityModel<DroneBatteryLevelAuditLog>> {

    @Override
    public EntityModel<DroneBatteryLevelAuditLog> toModel(DroneBatteryLevelAuditLog droneBatteryLevelAuditLog) {

        return EntityModel.of(droneBatteryLevelAuditLog,
                linkTo(methodOn(DispatchController.class).getDroneBatteryLevelLogs(droneBatteryLevelAuditLog.getId())).withSelfRel(),
                linkTo(methodOn(DispatchController.class).allDroneBatteryLevelLogs()).withRel("drone_battery_level_logs"));
    }
}
