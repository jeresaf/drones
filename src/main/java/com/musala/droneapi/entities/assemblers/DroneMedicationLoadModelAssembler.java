package com.musala.droneapi.entities.assemblers;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.*;

import com.musala.droneapi.controllers.DispatchController;
import com.musala.droneapi.entities.DroneMedicationLoad;
import com.musala.droneapi.entities.projections.DroneMedication;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

@Component
public class DroneMedicationLoadModelAssembler implements RepresentationModelAssembler<DroneMedicationLoad, EntityModel<DroneMedicationLoad>> {

    @Override
    public EntityModel<DroneMedicationLoad> toModel(DroneMedicationLoad droneMedicationLoad) {

        return EntityModel.of(droneMedicationLoad,
                linkTo(methodOn(DispatchController.class).getDroneMedicationLoad(droneMedicationLoad.getId())).withSelfRel(),
                linkTo(methodOn(DispatchController.class).allDroneMedicationLoads()).withRel("drone_medication_loads"));
    }
}
