package com.musala.droneapi.entities.assemblers;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.*;

import com.musala.droneapi.controllers.DispatchController;
import com.musala.droneapi.entities.Drone;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

@Component
public class DroneModelAssembler  implements RepresentationModelAssembler<Drone, EntityModel<Drone>> {

    @Override
    public EntityModel<Drone> toModel(Drone drone) {

        return EntityModel.of(drone,
                linkTo(methodOn(DispatchController.class).getDrone(drone.getId())).withSelfRel(),
                linkTo(methodOn(DispatchController.class).allDrones()).withRel("drones"));
    }
}
