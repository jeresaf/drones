package com.musala.droneapi.entities.assemblers;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.*;

import com.musala.droneapi.controllers.DispatchController;
import com.musala.droneapi.entities.Medication;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

@Component
public class MedicationModelAssembler implements RepresentationModelAssembler<Medication, EntityModel<Medication>> {

    @Override
    public EntityModel<Medication> toModel(Medication medication) {

        return EntityModel.of(medication, //
                linkTo(methodOn(DispatchController.class).getMedication(medication.getId())).withSelfRel(),
                linkTo(methodOn(DispatchController.class).allMedications()).withRel("medications"));
    }
}
