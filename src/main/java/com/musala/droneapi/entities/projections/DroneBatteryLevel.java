package com.musala.droneapi.entities.projections;

public interface DroneBatteryLevel {
    int getBattery_capacity();
}
