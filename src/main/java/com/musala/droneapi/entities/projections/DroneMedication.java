package com.musala.droneapi.entities.projections;

import com.musala.droneapi.entities.Medication;

public interface DroneMedication {
    Medication getMedication();
    int getQuantity();
}
