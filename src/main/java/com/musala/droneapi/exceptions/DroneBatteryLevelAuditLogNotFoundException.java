package com.musala.droneapi.exceptions;

public class DroneBatteryLevelAuditLogNotFoundException extends RuntimeException {

    public DroneBatteryLevelAuditLogNotFoundException(Long id) {
        super("Could not find battery level audit log for drone " + id);
    }
}
