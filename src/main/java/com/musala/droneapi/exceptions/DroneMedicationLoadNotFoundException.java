package com.musala.droneapi.exceptions;

public class DroneMedicationLoadNotFoundException   extends RuntimeException {

    public DroneMedicationLoadNotFoundException(Long id) {
        super("Could not find drone medication load " + id);
    }
}
