package com.musala.droneapi.exceptions;

public class DroneNotFoundException  extends RuntimeException {

    public DroneNotFoundException(Long id) {
        super("Could not find drone " + id);
    }
}
