package com.musala.droneapi.exceptions;

public class InvalidInputException   extends RuntimeException {

    public InvalidInputException(String message) {
        super(message);
    }
}
