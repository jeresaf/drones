package com.musala.droneapi.exceptions;

public class MedicationNotFoundException  extends RuntimeException {

    public MedicationNotFoundException(Long id) {
        super("Could not find medication " + id);
    }
}
