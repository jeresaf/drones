package com.musala.droneapi.models.request;

public class LoadMedicationRequest {

    private Long id;
    private int quantity;

    public LoadMedicationRequest() {}

    public LoadMedicationRequest(Long id, int quantity) {
        this.id = id;
        this.quantity = quantity;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
