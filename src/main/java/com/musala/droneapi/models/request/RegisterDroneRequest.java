package com.musala.droneapi.models.request;

import com.musala.droneapi.constants.Model;

public class RegisterDroneRequest {

    private String serial_number;
    private Model model;
    private double weight_limit;
    private int battery_capacity;

    public RegisterDroneRequest() {}

    public RegisterDroneRequest(String serial_number, Model model, double weight_limit, int battery_capacity) {
        this.serial_number = serial_number;
        this.model = model;
        this.weight_limit = weight_limit;
        this.battery_capacity = battery_capacity;
    }

    public String getSerial_number() {
        return serial_number;
    }

    public void setSerial_number(String serial_number) {
        this.serial_number = serial_number;
    }

    public Model getModel() {
        return model;
    }

    public void setModel(Model model) {
        this.model = model;
    }

    public double getWeight_limit() {
        return weight_limit;
    }

    public void setWeight_limit(double weight_limit) {
        this.weight_limit = weight_limit;
    }

    public int getBattery_capacity() {
        return battery_capacity;
    }

    public void setBattery_capacity(int battery_capacity) {
        this.battery_capacity = battery_capacity;
    }
}
