package com.musala.droneapi.repositories;


import com.musala.droneapi.constants.LoadState;
import com.musala.droneapi.entities.Drone;
import com.musala.droneapi.entities.DroneBatteryLevelAuditLog;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DroneBatteryLevelAuditLogRepository  extends JpaRepository<DroneBatteryLevelAuditLog, Long> {

    List<DroneBatteryLevelAuditLog> findByDrone(Drone drone);

}
