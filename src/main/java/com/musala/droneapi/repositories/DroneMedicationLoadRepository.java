package com.musala.droneapi.repositories;

import com.musala.droneapi.constants.LoadState;
import com.musala.droneapi.constants.State;
import com.musala.droneapi.entities.Drone;
import com.musala.droneapi.entities.DroneMedicationLoad;
import com.musala.droneapi.entities.Medication;
import com.musala.droneapi.entities.projections.DroneMedication;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface DroneMedicationLoadRepository extends JpaRepository<DroneMedicationLoad, Long> {

    List<DroneMedicationLoad> findByDroneAndMedicationAndLoadState(Drone drone, Medication medication, LoadState loadState);
    List<DroneMedicationLoad> findByDroneAndLoadState(Drone drone, LoadState loadState);

    @Query("SELECT dml FROM DroneMedicationLoad dml WHERE dml.drone = ?1 AND dml.loadState = ?2")
    List<DroneMedication> findDroneMedicationLoad(Drone drone, LoadState loadState);

}
