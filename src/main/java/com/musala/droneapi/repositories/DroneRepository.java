package com.musala.droneapi.repositories;

import com.musala.droneapi.constants.State;
import com.musala.droneapi.entities.Drone;
import com.musala.droneapi.entities.projections.DroneBatteryLevel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface DroneRepository extends JpaRepository<Drone, Long> {

    List<Drone> findByState(State state);

    @Query("SELECT d FROM Drone d WHERE d.id = ?1")
    DroneBatteryLevel findDroneBatteryCapacity(Long id);

}
