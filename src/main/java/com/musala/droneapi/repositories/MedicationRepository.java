package com.musala.droneapi.repositories;

import com.musala.droneapi.entities.Medication;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MedicationRepository extends JpaRepository<Medication, Long> {
}
