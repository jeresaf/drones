package com.musala.droneapi.scheduling;

import com.musala.droneapi.entities.Drone;
import com.musala.droneapi.entities.DroneBatteryLevelAuditLog;
import com.musala.droneapi.repositories.DroneBatteryLevelAuditLogRepository;
import com.musala.droneapi.repositories.DroneRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

@Component("scheduledDroneBatteryLevelCheck")
public class ScheduledDroneBatteryLevelCheck {

    DroneRepository droneRepository;
    DroneBatteryLevelAuditLogRepository droneBatteryLevelAuditLogRepository;

    public ScheduledDroneBatteryLevelCheck(DroneRepository droneRepository, DroneBatteryLevelAuditLogRepository droneBatteryLevelAuditLogRepository) {
        this.droneBatteryLevelAuditLogRepository = droneBatteryLevelAuditLogRepository;
        this.droneRepository = droneRepository;
    }
    private static final Logger log = LoggerFactory.getLogger(ScheduledDroneBatteryLevelCheck.class);

    @Scheduled(fixedDelayString = "${fixedDelay.in.milliseconds}")
    public void checkDroneBatteryLevel() {
        log.info("ScheduledDroneBatteryLevelCheck - " + System.currentTimeMillis() / 180000);
        List<Drone> droneList = droneRepository.findAll();
        for (Drone drone: droneList) {
            droneBatteryLevelAuditLogRepository.save(new DroneBatteryLevelAuditLog(drone, drone.getBattery_capacity()));
        }
    }

}
