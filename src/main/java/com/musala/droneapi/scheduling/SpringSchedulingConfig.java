package com.musala.droneapi.scheduling;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.EnableScheduling;

@Configuration
@EnableScheduling
@ComponentScan("com.musala.droneapi.scheduling")
@PropertySource("classpath:application.properties")
public class SpringSchedulingConfig { }