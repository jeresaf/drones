package com.musala.droneapi;

import com.musala.droneapi.controllers.DispatchController;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class DroneApiApplicationTests {

	@Autowired
	DispatchController dispatchController;

	@Test
	void contextLoads() {
		Assertions.assertThat(dispatchController).isNotNull();
	}

}
