package com.musala.droneapi.controllers;

import static org.mockito.Mockito.when;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Optional;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.musala.droneapi.constants.LoadState;
import com.musala.droneapi.constants.Model;
import com.musala.droneapi.constants.State;
import com.musala.droneapi.entities.Drone;
import com.musala.droneapi.entities.DroneMedicationLoad;
import com.musala.droneapi.entities.Medication;
import com.musala.droneapi.entities.assemblers.DroneBatteryLevelAuditLogModelAssembler;
import com.musala.droneapi.entities.assemblers.DroneMedicationLoadModelAssembler;
import com.musala.droneapi.entities.assemblers.DroneModelAssembler;
import com.musala.droneapi.entities.assemblers.MedicationModelAssembler;
import com.musala.droneapi.models.request.LoadMedicationRequest;
import com.musala.droneapi.models.request.RegisterDroneRequest;
import com.musala.droneapi.repositories.DroneBatteryLevelAuditLogRepository;
import com.musala.droneapi.repositories.DroneMedicationLoadRepository;
import com.musala.droneapi.repositories.DroneRepository;
import com.musala.droneapi.repositories.MedicationRepository;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.hateoas.EntityModel;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

@ExtendWith(SpringExtension.class)
@WebMvcTest(DispatchController.class)
public class DispatchControllerTests {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    DroneRepository droneRepository;

    @MockBean
    DroneModelAssembler droneModelAssembler;

    @MockBean
    MedicationRepository medicationRepository;

    @MockBean
    MedicationModelAssembler medicationModelAssembler;

    @MockBean
    DroneBatteryLevelAuditLogRepository droneBatteryLevelAuditLogRepository;

    @MockBean
    DroneBatteryLevelAuditLogModelAssembler droneBatteryLevelAuditLogModelAssembler;

    @MockBean
    DroneMedicationLoadRepository droneMedicationLoadRepository;

    @MockBean
    DroneMedicationLoadModelAssembler droneMedicationLoadModelAssembler;

    @Test
    public void testRegisterDrone() throws Exception {

        Drone drone2 = new Drone("JS1234KL", Model.LIGHTWEIGHT, 200, 100, State.IDLE);
        EntityModel<Drone> entityModel = EntityModel.of(drone2,
                linkTo(methodOn(DispatchController.class).getDrone(drone2.getId())).withSelfRel(),
                linkTo(methodOn(DispatchController.class).allDrones()).withRel("drones"));

        when(droneModelAssembler.toModel(droneRepository.save(drone2))).thenReturn(entityModel);

        RegisterDroneRequest registerDroneRequest = new RegisterDroneRequest("JS1234KL", Model.LIGHTWEIGHT, 200, 100);

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson = ow.writeValueAsString(registerDroneRequest);

        mockMvc.perform(post("/drones").contentType(APPLICATION_JSON)
                        .content(requestJson))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.serial_number", Matchers.is("JS1234KL")));
    }

    @Test
    public void testLoadDrone() throws Exception {

        Drone drone = new Drone("JS1234KL", Model.LIGHTWEIGHT, 200, 100, State.IDLE);
        drone.setId(1L);
        Medication medication = new Medication("Amoxicillin", 10, "AM109283", "amoxicillin_capsule_500.jpeg");
        medication.setId(1L);

        DroneMedicationLoad droneMedicationLoad =  new DroneMedicationLoad(drone, medication, LoadState.LOADED, 10);

        EntityModel<DroneMedicationLoad> entityModel = EntityModel.of(droneMedicationLoad,
                linkTo(methodOn(DispatchController.class).getDroneMedicationLoad(droneMedicationLoad.getId())).withSelfRel(),
                linkTo(methodOn(DispatchController.class).allDroneMedicationLoads()).withRel("drone_medication_loads"));

        when(droneMedicationLoadModelAssembler.toModel(droneMedicationLoadRepository.save(droneMedicationLoad))).thenReturn(entityModel);

        when(droneRepository.findById(1L)).thenReturn(Optional.of(drone));
        when(medicationRepository.findById(1L)).thenReturn(Optional.of(medication));

        LoadMedicationRequest loadMedicationRequest = new LoadMedicationRequest(1L, 10);

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();

        String requestJson = ow.writeValueAsString(loadMedicationRequest);

        mockMvc.perform(post("/drones/{id}/load_medication", 1L).contentType(APPLICATION_JSON)
                        .content(requestJson))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.drone.id", Matchers.is(1)));

    }

}
