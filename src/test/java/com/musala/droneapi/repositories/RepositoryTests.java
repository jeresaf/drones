package com.musala.droneapi.repositories;

import com.musala.droneapi.constants.LoadState;
import com.musala.droneapi.constants.Model;
import com.musala.droneapi.constants.State;
import com.musala.droneapi.entities.Drone;
import com.musala.droneapi.entities.DroneBatteryLevelAuditLog;
import com.musala.droneapi.entities.DroneMedicationLoad;
import com.musala.droneapi.entities.Medication;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class RepositoryTests {

    @Autowired
    DroneBatteryLevelAuditLogRepository droneBatteryLevelAuditLogRepository;

    @Autowired
    DroneMedicationLoadRepository droneMedicationLoadRepository;

    @Autowired
    DroneRepository droneRepository;

    @Autowired
    MedicationRepository medicationRepository;

    @Test
    public void testDroneCRUD() {
        Drone drone = new Drone("JS1234KL", Model.LIGHTWEIGHT, 200, 100, State.IDLE);
        drone = droneRepository.save(drone);
        Assertions.assertThat(drone.getId()).isNotNull();

        Iterable<Drone> drones = droneRepository.findAll();
        Assertions.assertThat(drones).isNotEmpty();
        Assertions.assertThat(drones).first().hasFieldOrPropertyWithValue("serial_number", "JS1234KL");

        drone.setBattery_capacity(98);
        drone = droneRepository.save(drone);
        Assertions.assertThat(drone).hasFieldOrPropertyWithValue("battery_capacity", 98);

        droneRepository.deleteAll();
        Assertions.assertThat(droneRepository.findAll()).isEmpty();
    }

    @Test
    public void testMedicationCRUD() {
        Medication medication = new Medication("Amoxicillin", 10, "AM109283", "amoxicillin_capsule_500.jpeg");
        medication = medicationRepository.save(medication);
        Assertions.assertThat(medication.getId()).isNotNull();

        Iterable<Medication> medications = medicationRepository.findAll();
        Assertions.assertThat(medications).isNotEmpty();
        Assertions.assertThat(medications).first().hasFieldOrPropertyWithValue("code", "AM109283");

        medication.setWeight(8);
        medication = medicationRepository.save(medication);
        Assertions.assertThat(medication).hasFieldOrPropertyWithValue("weight", 8.0);

        medicationRepository.deleteAll();
        Assertions.assertThat(medicationRepository.findAll()).isEmpty();
    }

    @Test
    public void testDroneMedicationLoadCRD() {

        Drone drone = new Drone("JS5463NM", Model.MIDDLEWEIGHT, 300, 100, State.IDLE);
        drone = droneRepository.save(drone);
        Assertions.assertThat(drone.getId()).isNotNull();

        Medication medication = new Medication("Aspirin", 5, "M893947", "aspirin.jpeg");
        medication = medicationRepository.save(medication);
        Assertions.assertThat(medication.getId()).isNotNull();

        DroneMedicationLoad droneMedicationLoad = new DroneMedicationLoad(drone, medication, LoadState.LOADED, 10);
        droneMedicationLoad = droneMedicationLoadRepository.save(droneMedicationLoad);
        Assertions.assertThat(droneMedicationLoad.getId()).isNotNull();

        Iterable<DroneMedicationLoad> droneMedicationLoads = droneMedicationLoadRepository.findAll();
        Assertions.assertThat(droneMedicationLoads).isNotEmpty();
        Assertions.assertThat(droneMedicationLoads).first().hasFieldOrPropertyWithValue("medication", medication);

        droneMedicationLoadRepository.deleteAll();
        Assertions.assertThat(droneMedicationLoadRepository.findAll()).isEmpty();

    }

    @Test
    public void testDroneBatteryLevelAuditLogCRUD() {
        Drone drone = new Drone("JS4390BC", Model.CRUISERWEIGHT, 400, 100, State.IDLE);
        drone = droneRepository.save(drone);
        Assertions.assertThat(drone.getId()).isNotNull();

        DroneBatteryLevelAuditLog droneBatteryLevelAuditLog = new DroneBatteryLevelAuditLog(drone, drone.getBattery_capacity());
        droneBatteryLevelAuditLog = droneBatteryLevelAuditLogRepository.save(droneBatteryLevelAuditLog);
        Assertions.assertThat(droneBatteryLevelAuditLog.getId()).isNotNull();

        drone.setBattery_capacity(98);
        drone = droneRepository.save(drone);
        Assertions.assertThat(drone).hasFieldOrPropertyWithValue("battery_capacity", 98);

        droneBatteryLevelAuditLog = new DroneBatteryLevelAuditLog(drone, drone.getBattery_capacity());
        droneBatteryLevelAuditLog = droneBatteryLevelAuditLogRepository.save(droneBatteryLevelAuditLog);
        Assertions.assertThat(droneBatteryLevelAuditLog.getId()).isNotNull();

        Iterable<DroneBatteryLevelAuditLog> droneBatteryLevelAuditLogs = droneBatteryLevelAuditLogRepository.findAll();
        Assertions.assertThat(droneBatteryLevelAuditLogs).isNotEmpty();
        Assertions.assertThat(droneBatteryLevelAuditLogs).first().hasFieldOrPropertyWithValue("battery_capacity", 100);
        Assertions.assertThat(droneBatteryLevelAuditLogs).last().hasFieldOrPropertyWithValue("battery_capacity", 98);

        droneMedicationLoadRepository.deleteAll();
        Assertions.assertThat(droneMedicationLoadRepository.findAll()).isEmpty();
    }

}
